    Irtaza Ali Sadiq Arain

== Practica 4: Práctica: DNS


== Objectiu de la pràctica
Configurar una màquina com a servidor DNS principal i una altra com a DNS
secundari de la xarxa local.

== Preparatius

* Els nostres servidors DNS resoldran els noms de tots els servidors i
routers de la xarxa local, i redirigiran les altres consultes a servidors
DNS externs.

* Crea una màquina virtual derivada de base i anomena-la Zoidberg.

* A Zoidberg posa-li dues interfícies web, una configurada com a NAT i l’altra com a xarxa interna (intnet).

* Configura la interfície NAT amb DHCP i la interna amb la IP 172.30.100.1.

* Seguint els passos de la pràctica anterior, totes les màquines han de mostrar
un prompt personalitzat i hi ha d’haver un usuari amb el vostre nom i permisos
per utilitzar sudo.

== Apartat 1: Instal·lació del servidor DNS a Zoidberg


El primer que farem serà comprovar la configuració DNS actual de Zoidberg.
Prova a consultar l’adreça IP del nom de domini louvre.fr. Per fer a mà una
consulta DNS podem utilitzar les comandes dig o nslookup.

.Entregar 
====
* Captura del resultat de la consulta DNS.

image::Screenshot_1.png[]

* Captura de la instrucció que instal·la el servidor DNS.

image::Screenshot_2.png[]

* Captura del resultat de la instrucció netstat que mostra el servidor DNS
funcionant.

image::Screenshot_3.png[]

====
Ara ja podem modificar la configuració de xarxa de Zoidberg per tal
que s’utilitzi a sí mateix com a servidor DNS.

* Modifica el fitxer netplan i escriu-hi l’adreça de Zoidberg (servidor DNS).

* Recorda fer netplan apply per a què els canvis es produeixin.

.Entregar 
====
* Captura de la execució de systemd-resolve --status .

image::Screenshot_4.png[]

====
Anem a provar el nou servei. El programa rndc s’utilitza per controlar
l’execució del bind. Anem a veure què passa exactament quan fem una
consulta al nostre DNS.

* Utilitza rndc per activar la depuració del servidor. Posa la depuració
a nivell 3 (com més gran aquest nombre, més informació es guarda al registre.

.Entregar 
====
* Captura de la instrucció utilitzada..

image::Screenshot_5.png[]

====

Com que la nostra xarxa no utilitza IPv6, indicarem al servidor DNS que només
treballa amb IPv4. Aquest pas no és imprescindible, però fer això reduïrà la
quantitat de missatges que apareixeran als registres, cosa que ens facilitarà
la posterior configuració del servidor.

Per fer que el servidor DNS només treballi amb IPv4 modificarem el fitxer
/etc/default/named i canviarem la línia OPTIONS de OPTIONS="-u bind" a
OPTIONS="-4 -u bind".

.Entregar 
====
* Captura de /etc/default/named.

image::Screenshot_6.png[]

====
Amb l’objectiu de reduir encara més els missatges no desitjats del registre, deshabilitarem la verificació dnssec.


Per fer que el servidor DNS no mostri error en verificar la identitat dels servidors superiors, modificarem l’arxiu named.conf.options i modificarem la línia dnssec-validation auto a dnssec-validation no.

Finalment per a què el registre mostri les consultes realitzades i l’origen de la consulta, afegirem al mateix arxiu named.conf.options la línia querylog yes;

Com sempre, recordem que cal reiniciar el servidor DNS cada cop que en modifiquem
la configuració per tal que s’adapti a la noves opcions.

.Entregar 
====
* Captura de /etc/bind/named.conf.options.

image::Screenshot_7.png[]

====

* Consulta la IP d’un nou domini, per exemple britishmuseum.org.


.Entregar 
====
* Captura de la instrucció utilitzada i el seu resultat.

image::Screenshot_8.png[]

====

Comprova la informació de depuració que ha quedat guardada al registre del
bind sobre aquesta consulta.


.Entregar 
====
* Part del registre del bind corresponent a l’última petició.

image::Screenshot_9.png[]

====

* Bolca el cau del servidor DNS a un fitxer utilitzant rndc i comprova quina informació s’ha
guardat sobre britishmuseum.org.

.Entregar 
====
*  Instrucció per bolcar el cau a un fitxer i contingut obtingut.

image::Screenshot_10.png[]

====

* Desactiva ara la depuració del servidor (rndc).

.Entregar 
====
*  Instrucció per desactivar la depuració del servidor DNS.

image::Screenshot_11.png[]

====


== Apartat 2: Configuració d’un servidor DNS només cache

Cada vegada que resolem un nom de domini a una adreça IP es produeix una
consulta a un servidor DNS. Això fa que per organitzacions petites valgui la
pena tenir un servidor DNS encara que només serveixi per fer de cau de les
consultes DNS que es van realitzant.

Amb un servidor així, la primera consulta contra un domini es passaria a un servidor DNS extern, però les següents consultes al mateix domini ja no caldria
que sortissin de la xarxa local (cosa que escurçaria el temps d’espera general
de les consultes).

En aquest apartat configurarem el servidor DNS per tal que actuï com un
cau DNS.


Canvis que cal fer a /etc/bind/named.conf.options:

* Configura el servidor per tal que no resolgui peticions per IPv6.

* Crea una llista de control d’accés per tal que només els clients de la nostra
xarxa puguin utilitzar el servidor DNS.


== Apartat 3: Configuració d’un servidor DNS forwarding

.Entregar 
====
*  Captura de les modificacions fetes a la configuració.

image::Screenshot_12.png[]

image::Screenshot_13.png[]

====

* Comprova que la configuració està ben escrita utilitzant named-checkconf.
Si no dóna cap resposta significa que no hi ha errors (cosa que no significa que
el servidor faci el que vols, només que la seva configuració està ben escrita).
És convenient utilitzar sovint aquesta ordre per assegurar-se que els canvis que
anem fent són vàlids.

.Entregar 
====
*  Sentències utilitzades i resultats.

image::Screenshot_14.png[]

====

== Apartat 4: Configuració dels clients

Ara que ja tenim el servidor Zoidberg funcionant com a servidor DNS, i que
d’aquí a poc també tindrem el servidor Wong com a DNS secundari, és el moment
de configurar tota la xarxa per tal que utilitzi aquests servidors.

* Configura a mà el propi Zoidberg i Wong, per tal que
utilitzin a Zoidberg i a Wong com a servidors DNS.

* Et pot resultar útil configurar un servidor DHCP per tal que els clients amb IP dinàmica també
utilitzin els nostres servidors DNS.


.Entregar 
====
*  Comprovació a Zoidberg i a PC1..

image::Screenshot_15.png[]
image::Screenshot_16.png[]

====


== Apartat 5: Creació de la zona interna

.Entregar 
====
*  /etc/bind/named.conf.local.

image::Screenshot_17.png[]

====


== Apartat 6: Configuració de la resolució directa

.Entregar 
====
*  Configuració de la resolució directa.

image::Screenshot_18.png[]

====

== Apartat 7: Configuració de la resolució inversa


.Entregar 
====
*  Configuració de la resolució inversa.

image::Screenshot_19.png[]

====

== Apartat 8:Comprovació de la configuració


.Entregar 
====
*  named-checkconf

image::Screenshot_20.png[]

* named-checkzone

image::Screenshot_21.png[]

* named-checkzone

image::Screenshot_22.png[]

* dig ns1.irtaza.sadiq.lan

image::Screenshot_23.png[]

* dig www.google.com

image::Screenshot_24.png[]

====



