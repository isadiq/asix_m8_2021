    Irtaza Ali Sadiq Arain

== Practica 3: Práctica: servidor DHCP


== Objectiu de la pràctica
Configurar una màquina com a servidor DHCP i una màquina com a clients del
servei.

== Preparatius

* Crea un clon de base i canvia-li el nom a Farnsworth.

* A Farnsworth posa-li dues interfícies web, una configurada com a NAT i l’altra com a xarxa interna (intnet).

* Configura la interfície NAT amb DHCP i la interna amb la IP 172.30.1.1.

* Seguint els passos de la pràctica anterior, totes les màquines han de mostrar
un prompt personalitzat i hi ha d’haver un usuari amb el vostre nom i permisos
per utilitzar sudo.

== Apartat 1: Instal·lació del servidor DHCP a Farnsworth

* Cerqueu el nom del paquet que conté el servidor DHCP a Debian/Ubuntu i instal·leu-lo.

El servidor DHCP no funciona directament quan s’instal·la. Cal modificar
dos fitxers de configuració per indicar les opcions que volem.

El primer fitxer és /etc/default/isc-dhcp-server. Aquí cal modificar el
paràmetre INTERFACES per indicar que s’han de servir IP dinàmiques a
les peticions que arribin a la interfície de xarxa (interna) que té Farnsworth.


.Entregar 
====
* Captura del contingut del fitxer /etc/default/isc-dhcp-server. 

image::Screenshot_1.png[]

====

*Instrucció utilitzada per reiniciar el servidor.*

* Inseriu una captura del registre del sistema mostrant els missatges que
llença el servidor DHCP.

====
sudo cat /var/log/syslog | grep dhcpd

====

.Entregar 
====
* Captura del resultat de la instrucció anterior

image::Screenshot_2.png[]

====

== Apartat 2: Servidor DHCP a la mateixa xarxa

Volem configurar el servidor DHCP per tal que doni IP als ordinadors de la
xarxa 172.30.1.0/24.

* El servidor DHCP proporcionarà la següent configuració a totes les màquines
client, independentment del seu segment de xarxa:

. Servidor DNS primari: 172.30.1.1

. Servidor DNS secundari: 172.30.1.2

. Nom de domini: cognom1-cognom2.test. Per exemple en Pere Roca Grau tindrà un
nom de domini roca-grau.test.

. Temps de concessió per defecte: una hora.

. Temps de concessió màxim: dues hores.

* Per a la xarxa 172.30.1.0/24, configurarem les següents opcions:

. EL servei DHCP repartirà adreces IP en el rang 172.30.1.100 –
172.30.1.254 (ambdues incloses).

. La porta d’enllaç serà 172.30.1.1.

.Entregar 
====
* Arxiu de configuració complet, esborreu tots els comentaris i feu-lo llegible.

image::Screenshot_3.png[]

====

== Apartat 3: Configuració de la màquina client PC1 (a la mateixa xarxa)

* Connectem temporalment la màquina PC1 a la xarxa 172.30.1.0/24. Això ho fem
directament des del VirtualBox, connectant la seva targeta de xarxa al
switch intnet.

* Configurem la xarxa de la màquina PC1 de manera que enp0s3 adquireixi IP
mitjançant DHCP. Hauria d’estar així per defecte.

.Entregar 
====
* Mostrar configuració de xarxa de PC1

image::Screenshot_5.png[]

====

* A Farnsworth seguiu els canvis al fitxer de registre del sistema amb tail -f
/var/log/syslog.

* Forceu la renovació de l’adreça IP a PC1 amb dhclient.

.Entregar 
====
* Comanda per demanar una nova IP amb dhclient

image::Screenshot_6.png[]

* Captura de pantalla amb la sortida del tcpdump.

image::Screenshot_7.png[]

====

== Apartat 4: Reserva d’adreces IP

* A la màquina Farnsworth modifiqueu la configuració del servei DHCP. Volem que
les següents màquines (fícticies) de la subxarxa 172.30.1.0/24 tinguin una IP
reservada fixa:

. SERVIDOR_WEB: Té la MAC 00:0c:76:8b:c4:16. Volem que se li assigni la IP
172.30.1.5.
. La màquina SERVIDOR_WEB ha de rebre el servidor DNS 8.8.8.8 i la porta
d’enllaç 10.0.0.1.
. PC1 ha de rebre la IP 172.30.1.8.

.Entregar 
====
* Captura de la configuració

image::Screenshot_9.png[]

====

* Reinicieu el servei DHCP a Farnsworth, i demaneu una renovació d’adreça a PC1.
Verifiqueu a PC1 que l’adreça adquirida és l’esperada.


.Entregar 
====
* Captura de la instrucció per validar l’adreça IP i del seu resultat.

image::Screenshot_10.png[]

====

== Apartat 5: Grups d’adreces i clients registrats

* Assumirem ara que a la xarxa 172.30.1.0/24 es barregen màquines conegudes i
registrades (en sabem la MAC i la desem a l’arxiu de configuració) i
desconegudes (imagineu, per exemple, clients WIFI dels que no podem saber la
MAC).

* El DHCP assignarà:

 . el rang 172.30.1.30 a 172.30.1.150 a les màquines registrades al DHCP.
 . el rang 172.30.1.151 a 172.30.1.254 per a les màquines no registrades.
 
* Per a totes les màquines d’aquesta subxarxa, la porta d’enllaç és la
172.30.1.1, i els DNS són 172.30.1.1 i 172.30.1.2.

* Per facilitar la feina, només definirem 2 màquines amb adreces MAC
registrades, seran PC1 i OFICINA. Inventeu-vos la MAC d’OFICINA.

.Entregar 
====
* Captura de les modificacions de l’arxiu de configuració.

image::Screenshot_11.png[]

====

* Renoveu la IP de PC1 i verifiqueu que adquireix una IP del rang registrat.
* Situeu PC2 a la mateixa xarxa i comprovem quina IP agafa. Verifiqueu que
adquireix una IP del rang NO registrat.

.Entregar 
====
* Captura del terminal amb la renovació i la IP adquirida.

* Captura del terminal amb la renovació i la IP adquirida.

image::Screenshot_12.png[]

====

== Apartat 6: Configuració de PC1 i PC2 a una xarxa diferent

Volem ara traslladar els ordinadors PC1 i PC2 a la xarxa 172.30.2.0/24.
Tindràs que configurar les seves interfícies internes a VirtualBox amb el nom intnet2

* Afegiu al servidor DHCP la configuració de la xarxa 172.30.2.0/24:

. EL servei DHCP repartirà adreces IP en el rang 172.30.2.100 –
172.30.2.254 (ambdues incloses).

. La porta d’enllaç serà 172.30.2.1.

* Esborra de la configuració qualsevol referència a la MAC de PC1 per evitar
conflictes.

.Entregar 
====
* Captura de la nova configuració.

image::Screenshot_13.png[]

* Comprovació de la IP de PC1 i PC2.

image::Screenshot_14.png[]

image::Screenshot_15.png[]

====

== Apartat 7: Configuració d’un servidor secundari


.Entregar 
====
* Instal·lació del NTP a Farnsworth i a Hermes.

image::Screenshot_16.png[]


====













